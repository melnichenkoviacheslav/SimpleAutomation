﻿namespace SimpleAutomationTests
{
    using System;
    using Atata;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using SimpleAutomationCommon.DataModels.Enums;
    using SimpleAutomationCommon.Helpers;
    using LogLevel = Atata.LogLevel;

    public class BaseTest
    {
        private AtataContextBuilder _contextBuilder;

        [OneTimeSetUp]
        protected void GeneralSetUp()
        {
            _contextBuilder = AtataContext.Configure();
            SetupDriver();
            _contextBuilder.Build();
            AtataContext.Current.Driver.Maximize();
        }

        [OneTimeTearDown]
        protected void GeneralTearDown()
        {
            AtataContext.Current?.CleanUp();
        }

        [SetUp]
        protected void SetUp()
        {
        }

        [TearDown]
        protected void TearDown()
        {
        }

        private void SetupDriver()
        {
            var browserName = ConfigurationHelper.Browser;
            if (ConfigurationHelper.IsTeamCityRun)
            {
                SetupRemoteDriver(browserName);
            }
            else
            {
                SetUpLocalDriver(browserName);
            }

            _contextBuilder
                .UseBaseUrl(ConfigurationHelper.MainUrl)
                .UseElementFindTimeout(ConfigurationHelper.ElementTimeOut)
                .UseWaitingRetryInterval(ConfigurationHelper.RetryTimeOut)
                .UseCulture("en-us")
                .AddNUnitTestContextLogging()
                .WithMinLevel(LogLevel.Debug)
                .UseNUnitTestName()
                .TakeScreenshotOnNUnitError()
                .AddScreenshotFileSaving()
                .WithFolderPath(() =>
                    $@"ScreenShots\{ConfigurationHelper.ScreenshotsFolderPath}{AtataContext.BuildStart:yyyy-MM-dd-HH-mm-ss}")
                .WithFileName(screenshotInfo => $"{screenshotInfo.PageObjectName}_{AtataContext.Current.TestName}");
        }

        private void SetUpLocalDriver(Browser browserName)
        {
            switch (browserName)
            {
                case Browser.Chrome:
                    _contextBuilder
                        .UseChrome()
                        .WithFixOfCommandExecutionDelay()
                        .WithArguments("start-maximized", "disable-infobars",
                            $"--homepage=${ConfigurationHelper.MainUrl}", "disable-extensions")
                        .WithOptions(x => x.AddUserProfilePreference("credentials_enable_service", false));
                    break;
                case Browser.Firefox:
                    _contextBuilder
                        .UseFirefox()
                        .WithFixOfCommandExecutionDelay();
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"No such browser registered as:{browserName}");
            }
        }

        private void SetupRemoteDriver(Browser browserName)
        {
            var capabilities = new DesiredCapabilities();
            capabilities.Platform = new Platform(PlatformType.Any);
            capabilities.SetCapability("enableVNC", ConfigurationHelper.Vnc);
            capabilities.SetCapability("enableVideo", ConfigurationHelper.EnableVideoRecording);
            capabilities.SetCapability("screenResolution", "1920x1080x24");
            capabilities.SetCapability(CapabilityType.BrowserName, ConfigurationHelper.Browser.ToString().ToLower());
            capabilities.SetCapability(CapabilityType.Version, ConfigurationHelper.BrowserVersion);

            _contextBuilder
                .UseRemoteDriver()
                .WithRemoteAddress(new Uri(ConfigurationHelper.SelenoidHub))
                .WithCapabilities(capabilities);
        }
    }
}