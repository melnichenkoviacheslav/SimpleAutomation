﻿namespace SimpleAutomationTests.Tests
{
    using Atata;
    using FluentAssertions;
    using NUnit.Framework;
    using SimpleAutomationCommon.Pages.LoginPg;

    public class FooterTests : BaseTest
    {
        [Test]
        public void AboutUsTest()
        {
            var aboutUsPage = Go.To<LoginPage>().AboutUs.ClickAndGo();
            var title = aboutUsPage.GetTitle();
            title.Should().Be("About Us");
        }

        [Test]
        public void HelpCenterTest()
        {
            var helpCenterPage = Go.To<LoginPage>().HelpCenter.ClickAndGo();
            var title = helpCenterPage.GetTitle();
            title.Should().Be("Help center");
        }

        [Test]
        public void TermsOfUSeTest()
        {
            var touPage = Go.To<LoginPage>().TermsOfUse.ClickAndGo();
            var title = touPage.GetTitle();
            title.Should().Be("Term and Conditions");
        }

        [Test]
        public void WomanTest()
        {
            var womanPage = Go.To<LoginPage>().Woman.ClickAndGo();
            var title = womanPage.GetTitle();
            title.Should().Be("Woman");
        }

        [Test]
        public void ManTest()
        {
            var manPage = Go.To<LoginPage>().Man.ClickAndGo();
            var title = manPage.GetTitle();
            title.Should().Be("Man");
        }

        [Test]
        public void ShoesTest()
        {
            var shoesPage = Go.To<LoginPage>().Shoes.ClickAndGo();
            var title = shoesPage.GetTitle();
            title.Should().Be("Shoes");
        }

        [Test]
        public void WatchesTest()
        {
            var watchesPage = Go.To<LoginPage>().Watches.ClickAndGo();
            var title = watchesPage.GetTitle();
            title.Should().Be("Watches");
        }
    }
}
