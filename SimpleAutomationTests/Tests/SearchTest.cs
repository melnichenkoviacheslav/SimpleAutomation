﻿using System.Linq;
using Atata;
using FluentAssertions;
using NUnit.Framework;
using SimpleAutomationCommon.Helpers.Extensions;
using SimpleAutomationCommon.Pages;

namespace SimpleAutomationTests.Tests
{
    public class SearchTest : BaseTest
    {
        [Test]
        public void WemanClothesSearch()
        {
            var womanPage = Go.To<HomePage>().Woman.ClickAndGo();

            womanPage.GetTitle().Should().Be("Woman");

            womanPage.GetResult().Should().Be("results");
        }

        [Test]
        public void MenClothesSearch()
        {
            var manPage = Go.To<HomePage>().Man.ClickAndGo();

            manPage.GetTitle().Should().Be("Man");

            manPage.GetResult().Should().Be("results");
        }

        [Test]
        public void ShoesSearch()
        {
            var shoesPage = Go.To<HomePage>().Shoes.ClickAndGo();

            shoesPage.GetTitle().Should().Be("Shoes");

            shoesPage.GetResult().Should().Be("results");
        }

        [Test]
        public void WatchesSearch()
        {
            var watchesPage = Go.To<HomePage>().Whatches.ClickAndGo();

            watchesPage.GetTitle().Should().Be("Watches");

            watchesPage.GetResult().Should().Be("results");
        }

        [Test]
        public void MenTshirtsSearch()
        {
            var manSubMenu = Go.To<HomePage>().Man.Hover();
            manSubMenu.Tshirt.IsVisible.Should.BeTrue();

            var tshirtPage = manSubMenu.Tshirt.ClickAndGo();

            tshirtPage.GetTitle().Should().Be("T-Shirt");

            tshirtPage.GetResult().Should().Be("results");
        }

        [Test]
        public void MenSuitsSearch()
        {
            var manSubMenu = Go.To<HomePage>().Man.Hover();
            manSubMenu.Suit.IsVisible.Should.BeTrue();

            var suitPage = manSubMenu.Suit.ClickAndGo();

            suitPage.GetTitle().Should().Be("Suit");

            suitPage.GetResult().Should().Be("results");
        }

        [Test]
        public void CloseSearchPopup()
        {
            var searchPopup = Go.To<HomePage>().OpenSearchButton.Click();
            searchPopup.SearchField.IsVisible.Should.BeTrue();

            searchPopup.HideSearchButton.Click();
            searchPopup.SearchField.IsVisible.Should.BeFalse();
        }

        [Test]
        public void SearchParticularGood()
        {
            var homePage = Go.To<HomePage>();
            var searchPopup = homePage.OpenSearchButton.Click();
            searchPopup.SearchField.IsVisible.Should.BeTrue();

            searchPopup.SearchField.Set("Vintage Inspired Classic");
            var searchPage = searchPopup.SearchButton.ClickAndGo();

            searchPage.GetResult().Should().BeEquivalentTo("results");

            searchPage.Products.Count().Should().Be(1);
            searchPage.Products.Count().Should().Be(searchPage.GetCountOfResults());

            searchPage
                .Products
                .ForEach(x => x.ProductName.Get().Should().Be("Vintage Inspired Classic"));
        }

        [Test]
        public void SearchSeveralGoods()
        {
            var homePage = Go.To<HomePage>();
            var searchPopup = homePage.OpenSearchButton.Click();
            searchPopup.SearchField.IsVisible.Should.BeTrue();

            searchPopup.SearchField.Set("Classic");
            var searchPage = searchPopup.SearchButton.ClickAndGo();

            searchPage.GetSearchKeywordFromTitle().Should().Be("Classic");

            searchPage.GetResult().Should().BeEquivalentTo("results");

            searchPage.Products.Count().Should().Be(2);
            searchPage.Products.Count().Should().Be(searchPage.GetCountOfResults());

            searchPage
                .Products
                .ForEach(x => x.ProductName.Get().Should().Contain(searchPage.GetSearchKeywordFromTitle()));
        }

        [Test]
        public void SearchNonexistentGood()
        {
            var homePage = Go.To<HomePage>();
            var searchPopup = homePage.OpenSearchButton.Click();
            searchPopup.SearchField.IsVisible.Should.BeTrue();

            searchPopup.SearchField.Set("Nonexistentgood");
            var searchPage = searchPopup.SearchButton.ClickAndGo();

            searchPage.GetSearchKeywordFromTitle().Should().Be("Nonexistentgood");

            searchPage.GetResult().Should().BeEquivalentTo("results");

            searchPage.Products.Count().Should().Be(0);
            searchPage.Products.Should().BeEmpty();
        }
    }
}