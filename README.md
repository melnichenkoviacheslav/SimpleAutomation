# Simple Automation

This is the sample project that cover [SimplCommerce](https://github.com/simplcommerce) open source project.

It contains UI tests that uses selenium based [ATATA](https://atata-framework.github.io/) framework.

## Getting Started
Next step depence on environment you use to run tests.

- install/update *dotnet till SDK 2.2*.

## Run With Visual Studio:
- add VS extension *Nunit Test Adapter*;
- clone and build the project;
- make sure *driver* files are in bin folder or add it to env path.

## Run With CI:
This framework uses selenium RemoteDriver for driver initializing.
Choose own setup for appsettings file or use next simple example.
The simplest way to run it with in docker container.
- Install docker.
- Use [Selenoid](https://github.com/aerokube/selenoid) or [ConfigurationManager](https://aerokube.com/cm/latest/).
- Add appropriate env parameters for your CI build (all env variables in TestConfigurationHelper.cs).
- Use next command to start selenoid container:
```
cm selenoid start --vnc --browsers 'chrome:72.0;firefox:65.0' --args "-limit 4"
```
