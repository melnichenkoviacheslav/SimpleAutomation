﻿namespace SimpleAutomationCommon.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DataModels.Enums;
    using Extensions;
    using Microsoft.Extensions.Configuration;

    public static class ConfigurationHelper
    {
        private const string ChromeDefaultVersion = "72.0";
        private const string FireFoxDefaultVersion = "65.0";

        public static IEnumerable<string> BrowserOptions
            => (Environment.GetEnvironmentVariable("optionArguments")
                ?? string.Empty).Split(';').ToList();

        public static Browser Browser => Enum
            .Parse<Browser>(
                Environment.GetEnvironmentVariable("browser")
                             ?? GetSettings("Browser", "Name"), true);

        public static object BrowserVersion => Environment.GetEnvironmentVariable("version")
                                               ?? (Browser == Browser.Chrome
                                                   ? ChromeDefaultVersion
                                                   : FireFoxDefaultVersion);

        public static TimeSpan ElementTimeOut =>
            TimeSpan.FromSeconds(GetSettings("Browser", "elementTimeout").Split(':').Last().ToInt());

        public static bool EnableVideoRecording =>
            (Environment.GetEnvironmentVariable("enableVideoRecording") ?? "false").Equals("true");

        public static bool IsTeamCityRun => !Environment.GetEnvironmentVariable("simplCommerceEndpoint").IsNullOrEmpty();

        public static string MainUrl =>
            Environment.GetEnvironmentVariable("simplCommerceEndpoint") ?? GetSettings("App", "homeUrl");

        public static TimeSpan RetryTimeOut =>
            TimeSpan.FromMilliseconds(GetSettings("Browser", "retryTimeout").ToInt());

        public static string ScreenshotsFolderPath =>
                    Environment.GetEnvironmentVariable("screenShotsFolderPath") ?? string.Empty;

        public static string SelenoidHub => Environment.GetEnvironmentVariable("selenoidHub");

        public static bool Vnc =>
                    (Environment.GetEnvironmentVariable("vnc") ?? GetSettings("Browser", "vnc"))
            .ToLower().Equals("true");

        private static string GetSettings(string sectionName, string settingName)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .AddEnvironmentVariables()
                .Build();

            var setting = configuration.GetSection(sectionName);
            if (setting == null)
            {
                throw new Exception($"{settingName} setting is missing");
            }

            return setting[settingName];
        }
    }
}