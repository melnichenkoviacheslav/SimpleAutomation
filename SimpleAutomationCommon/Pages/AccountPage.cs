﻿namespace SimpleAutomationCommon.Pages
{
    using System.Collections.Generic;
    using System.Linq;
    using Atata;
    using DataModels;
    using DataModels.Users;
    using Helpers.Extensions;
    using _ = AccountPage;

    [Url("user")]
    public class AccountPage : Page<_>
    {
        [FindByXPath("//a[text()='Edit']")]
        private Text<_> EditAccount { get; set; }

        [FindByXPath("//div[.='Account Information']/following-sibling::div")]
        private Text<_> UserInfo { get; set; }

        public List<string> GetInfo()
            => UserInfo.Get().Remove("\r\nEdit", "\r").Split('\n').Select(i => i.Trim()).ToList();

        public User GetUser()
        {
            var accountInfo = GetInfo().SkipLast(1);
            return new User
            {
                Email = new Email(accountInfo.Last()),
                FullName = accountInfo.First()
            };
        }

        public bool IsLoaded()
            => EditAccount.IsEnabled;
    }
}