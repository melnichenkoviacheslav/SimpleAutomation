﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = AboutUsPage;

    [Url("about-us")]
    public class AboutUsPage : BasePage<_>
    {
        [FindByCss("h1")]
        private H1<_> AboutUsTitle { get; set; }

        public string GetTitle()
        {
            return AboutUsTitle.Get();
        }
    }
}
