﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = ManPage;

    [Url("man")]
    public class ManPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> ManTitle { get; set; }

        public string GetTitle()
        {
            return ManTitle.Get();
        }
    }
}
