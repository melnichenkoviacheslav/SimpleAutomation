﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = ShoesPage;

    [Url("shoes")]
    public class ShoesPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> ShoesTitle { get; set; }

        public string GetTitle()
        {
            return ShoesTitle.Get();
        }
    }
}
