﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _= SuitPage;

    [Url("suit")]
    public class SuitPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> SuitTitle { get; set; }

        public string GetTitle()
        => SuitTitle.Get();
    }
}
