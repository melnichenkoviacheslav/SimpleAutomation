﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = WathesPage;

    [Url("watches")]
    public class WathesPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> WatchesTitle { get; set; }

        public string GetTitle()
        {
            return WatchesTitle.Get();
        }
    }
}
