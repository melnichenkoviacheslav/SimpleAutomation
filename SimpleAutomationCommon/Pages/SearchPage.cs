﻿namespace SimpleAutomationCommon.Pages
{
    using System.Linq;
    using Atata;
    using _ = SearchPage;

    [Url("search")]
    public class SearchPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> SearchTitle { get; set; }

        public string GetSearchKeywordFromTitle()
            => SearchTitle.Get().SplitIntoWords().ToList().Last();

        public ControlList<ProductCard, _> Products { get; set; }

        [ControlDefinition("div", ContainingClass = "card mb-4")]
        public class ProductCard : Control<_>
        {
            public H5<_> ProductName { get; set; }
        }
    }
}
