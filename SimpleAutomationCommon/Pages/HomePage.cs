﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = HomePage;

    public class HomePage : BasePage<_>
    {
        [FindByXPath("//ul[@class='main-menu']//a[@href='/woman']")]
        public Link<WomanPage, _> Woman { get; set; }

        [FindByXPath("//ul[@class='main-menu']//a[@href='/man']")]
        public Link<ManPage, _> Man { get; set; }

        [FindByXPath("//a[@href='/t-shirt']")]
        public Link<TshirtPage, _> Tshirt { get; set; }

        [FindByXPath("//a[@href='/suit']")]
        public Link<SuitPage, _> Suit { get; set; }

        [FindByXPath("//ul[@class='main-menu']//a[@href='/shoes']")]
        public Link<ShoesPage, _> Shoes { get; set; }

        [FindByXPath("//ul[@class='main-menu']//a[@href='/watches']")]
        public Link<ShoesPage, _> Whatches { get; set; }
    }
}
