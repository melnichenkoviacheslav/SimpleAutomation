﻿using System.Collections.Generic;
using System.Linq;
using Atata;
using SimpleAutomationCommon.Helpers.Extensions;

namespace SimpleAutomationCommon.Pages
{
    public class BasePage<TOwner> : Page<TOwner>
        where TOwner : BasePage<TOwner>
    {
        [FindByCss("a[href = '/login']")]
        public Link<TOwner> Login { get; set; }

        [FindByCss("div.top-bar a[href = '/user']")]
        public Text<TOwner> FullName { get; set; }

        [FindByCss("a[href = '/register']")]
        public Link<TOwner> Register { get; set; }

        [FindByXPath("//a[contains(text(), 'Language')]")]
        public Label<TOwner> Language { get; set; }

        [FindByXPath("//img[@alt = 'SimplCommerce']")]
        public Label<TOwner> Logo { get; set; }

        [FindByXPath("(//div[contains(@class, 'show-modal-search')])[1]")]
        public Clickable<TOwner> OpenSearchButton { get; set; }

        [FindByCss("button[class*='search']")]
        public Button<TOwner> HideSearchButton { get; set; }

        [FindByName("query")]
        public TextInput<TOwner> SearchField { get; set; }

        [FindByCss("form[action*='search'] button")]
        public Button<SearchPage, TOwner> SearchButton { get; set; }

        [FindByXPath("//a[text()='Accessories']")]
        public Label<TOwner> Accessories { get; set; }

        [FindByXPath("//footer//a[@href='/woman']")]
        public Link<WomanPage, TOwner> Woman { get; set; }

        [FindByXPath("//footer//a[@href='/man']")]
        public Link<ManPage, TOwner> Man { get; set; }

        [FindByXPath("//footer//a[@href='/shoes']")]
        public Link<ShoesPage, TOwner> Shoes { get; set; }

        [FindByXPath("//footer//a[@href='/watches']")]
        public Link<WathesPage, TOwner> Watches { get; set; }

        [FindByXPath("//footer//a[@href='/help-center']")]
        public Link<HelpCenterPage, TOwner> HelpCenter { get; set; }

        [FindByXPath("//footer//a[@href='/about-us']")]
        public Link<AboutUsPage, TOwner> AboutUs { get; set; }

        [FindByXPath("//footer//a[@href='/terms-of-use']")]
        public Link<TermsOfUsePage, TOwner> TermsOfUse { get; set; }

        [FindByXPath("//p[text()='© 2018 - SimplCommerce']")]
        public Label<TOwner> CopyRights { get; set; }

        [FindByXPath("//p[text()='Powered by ']")]
        public Label<TOwner> PoweredBy { get; set; }

        [FindByClass("badge-results")]
        private Text<TOwner> Results { get; set; }

        public string GetUser()
        {
            var userName = FullName.Get();
            return userName.Remove("Hello ", "!");
        }

        public string GetResult()
            => Results.Get().SplitIntoWords().Last();

        public int GetCountOfResults()
            => Results.Get().SplitIntoWords().First().ToInt();
    }
}