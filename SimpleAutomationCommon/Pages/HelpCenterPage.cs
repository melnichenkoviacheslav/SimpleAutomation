﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = HelpCenterPage;

    [Url("help-center")]
    public class HelpCenterPage : BasePage<_>
    {
        [FindByCss("h1")]
        private H1<_> HelpCenterTitle { get; set; }

        public string GetTitle()
        {
            return HelpCenterTitle.Get();
        }
    }
}
