﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = WomanPage;

    [Url("woman")]
    public class WomanPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> WomanTitle { get; set; }

        public string GetTitle()
        {
            return WomanTitle.Get();
        }
    }
}
