﻿using Atata;

namespace SimpleAutomationCommon.Pages
{
    using _ = TermsOfUsePage;

    [Url("terms-of-use")]
    public class TermsOfUsePage : BasePage<_>
    {
        [FindByCss("h1")]
        private H1<_> TouTitle { get; set; }

        public string GetTitle()
        {
            return TouTitle.Get();
        }
    }
}
