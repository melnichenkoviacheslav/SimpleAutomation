﻿namespace SimpleAutomationCommon.Pages
{
    using Atata;
    using _ = TshirtPage;

    [Url("t-shirt")]
    public class TshirtPage : BasePage<_>
    {
        [FindByCss("h2")]
        private H2<_> TshirtTitle { get; set; }

        public string GetTitle()
            => TshirtTitle.Get();
    }
}
